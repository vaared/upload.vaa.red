<?php
define('permit_incl', TRUE);
class base58 {
	static public $alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	public static function encode($int) {
		$base58_string = '';
		$base = strlen(self::$alphabet);
		while($int >= $base) {
			$div = floor($int / $base);
			$mod = ($int - ($base * $div));
			$base58_string = self::$alphabet{$mod} . $base58_string;
			$int = $div;
		}
		if($int) $base58_string = self::$alphabet{$int} . $base58_string;
		return $base58_string;
	}
	public static function decode($base58) {
		$int_val = 0;
		for($i=strlen($base58)-1,$j=1,$base=strlen(self::$alphabet);$i>=0;$i--,$j*=$base) {
			$int_val += $j * strpos(self::$alphabet, $base58{$i});
		}
		return $int_val;
	}
}

function insertDb($id, $date_to_delete, $deletionId, $ip, $fileSize, $filename, $extension) {
    include('db.php');
    $sql = "INSERT INTO `data` (`id`, `when_to_delete`, `deletion_url`, `ip`, `filesize`, `filename`, `extension`)
    VALUES ('".$id."', '".$date_to_delete."', '".$deletionId."', '".$ip."', '".$fileSize."', '".$filename."', '".$extension."')";

    if ($conn->query($sql) !== TRUE) {
        $conn->close();
        die();
    }

    $conn->close();
}

function getUserIP() {
    if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
            $addr = explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']);
            return trim($addr[0]);
        } else {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    else {
        return $_SERVER['REMOTE_ADDR'];
    }
}

function cleanString($string) {
   $string = str_replace(' ', '-', $string);
   return preg_replace('/[^A-Za-z0-9\-.]/', '', $string);
}

if($_POST['encrypting'] == 'true') {
    $data = $_POST['encrypted_base64'];
    $data = htmlentities($data, ENT_QUOTES, 'UTF-8');
    if($data != '') {
        if($_POST['minutes'] == '' || $_POST['minutes'] == '0') {
            $timestamp = '2030-04-20 04:20:00';
        } else {
            $time = new DateTime();
            $time->add(new DateInterval('PT' . $_POST['minutes'] . 'M'));
            $timestamp = $time->format('Y-m-d H:i:s');
        }
        $metadata_ext = '';
        $metadata_filename = '';
        if($_POST['metadata_ext'] != '') {
            $metadata_ext = cleanString($_POST['metadata_ext']);
            $metadata_ext = htmlentities($metadata_ext, ENT_QUOTES, 'UTF-8');
        } elseif($_POST['metadata_name'] != '') {
            $metadata_filename = cleanString($_POST['metadata_name']);
            $metadatmetadata_filenamea_ext = htmlentities($metadata_filename, ENT_QUOTES, 'UTF-8');
        }
        $fileId = base58::encode(random_int(1, 1000000000));
        $deletionId = base58::encode(random_int(1, 1000000000)) . '' .  base58::encode(random_int(1, 1000000000)) . '' . base58::encode(random_int(1, 1000000000));
        $file = 'ie/' . $fileId . '.aes';
        file_put_contents($file, $data);
        $userIp = getUserIP();
        $fileSize = mb_strlen($data, '8bit');
        
        if($metadata_ext != '') {
            insertDb($fileId, $timestamp, $deletionId, $userIp, $fileSize, '', $metadata_ext);
        } else {
            if($metadata_filename != '') {
                insertDb($fileId, $timestamp, $deletionId, $userIp, $fileSize, $metadata_filename, '');
            } else {
                insertDb($fileId, $timestamp, $deletionId, $userIp, $fileSize, '', '');
            }
        }
        
        $output = array('message' => 'success', 'fileId' => $fileId, 'deletionId' => $deletionId);
        echo json_encode($output);
        die();
    } else {
        $output = array('message' => 'something went wrong');
        echo json_encode($output);
        die();
    }
} else {
    if($_POST['deletion_time'] === 'custom') {
        $minutes = (int)$_POST['user_deletion_time'];
    } else {
          if($_POST['deletion_time'] == '' || $_POST['deletion_time'] == 0) {
            $timestamp = '2030-04-20 04:20:00';
        } else {
            $time = new DateTime();
            $time->add(new DateInterval('PT' . $_POST['deletion_time'] . 'M'));
            $timestamp = $time->format('Y-m-d H:i:s');
        }
    }
    try {
        if (
            !isset($_FILES['upfile']['error']) ||
            is_array($_FILES['upfile']['error'])
        ) {
            throw new RuntimeException('something went wrong');
        }

        switch ($_FILES['upfile']['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('i need file!');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('exceeded filesize limit');
            default:
                throw new RuntimeException('something went wrong');
        }
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
            $finfo->file($_FILES['upfile']['tmp_name']),
            array(
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
            ),
            true
        )) {
            throw new RuntimeException('jpg, jpeg, png and gif are only allowed for unencrypted uploads');
        }

        if ($_FILES['upfile']['size'] > 31457280) {
            throw new RuntimeException('exceeded filesize limit');
        }
        
        $safe_name = sprintf('i/%s.%s', base58::encode(random_int(1, 1000000000)), $ext);
        if (!move_uploaded_file($_FILES['upfile']['tmp_name'], $safe_name)
        ) {
            throw new RuntimeException('something went wrong');
        }

        $deletionId = base58::encode(random_int(1, 1000000000)) . '' .  base58::encode(random_int(1, 1000000000)) . '' . base58::encode(random_int(1, 1000000000));
        $file_name = substr($safe_name, 2);
        $ip = getUserIP();
        insertDb($file_name, $timestamp, $deletionId, $ip, $_FILES['upfile']['size']);
        $output = array('message' => 'success', 'url' => 'https://upload.vaa.red/'.$safe_name.'', 'deletionId' => ''.$deletionId.'');
        echo json_encode($output);
        die();
    } catch (RuntimeException $e) {
        $output = array('message' => $e->getMessage());
        echo json_encode($output);
    }
}
?>
