<?php if (isset($_GET['id']) && $_GET['id'] != '/'): ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="theme-color" content="#a20000">
        <meta name="msapplication-navbutton-color" content="#a20000">
        <meta name="apple-mobile-web-app-status-bar-style" content="#a20000">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="robots" content="noindex">
        <link rel="icon" href="https://vaa.red/fav.png" sizes="96x96">
        <link rel="stylesheet" type="text/css" href="css/decrypt.min.css">
        <script src="js/app.decrypt.js"></script>
        <title>upload vaa – client-side encrypted file upload</title>
    </head>
    <body>
        <h1>attempting to decrypt...</h1>
        <?php
        session_start();
        define('permit_incl', TRUE);
        function getUserIP() {
            if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER) && !empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') > 0) {
                    $addr = explode(",",$_SERVER['HTTP_X_FORWARDED_FOR']);
                    return trim($addr[0]);
                } else {
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
            }
            else {
                return $_SERVER['REMOTE_ADDR'];
            }
        }

        function readfile_chunked($filename, $retbytes = TRUE) {
            $buffer = '';
            $cnt    = 0;
            $handle = fopen($filename, 'rb');

            if ($handle === false) {
                return false;
            }

            while (!feof($handle)) {
                $buffer = fread($handle, 1024*1024);
                echo $buffer;
                ob_flush();
                flush();
                if ($retbytes) {
                    $cnt += strlen($buffer);
                }
            }

            $status = fclose($handle);

            if ($retbytes && $status) {
                return $cnt;
            }

            return $status;
        }

        function getFileMetadata($fileId) {
            include('db.php');
            $sql="SELECT filename, extension FROM data WHERE id = '".$fileId."'"; 
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $metadata["filename"] = $row["filename"];
                    $metadata["extension"] = $row["extension"];
                    return $metadata;
                }
            }
        }

        $fileId = $_GET['id'];
        if(preg_match("/^[a-zA-Z0-9]+$/", $fileId) == 1) {
            $path = 'ie/' . $fileId . '.aes';
            if(file_exists('/var/www/upload/' . $path)) {
                echo '<pre style="display: none;">';
                    $data = readfile_chunked('/var/www/upload/' . $path);
                echo '</pre>';
                $metadata = getFileMetadata($fileId);
                if($metadata["filename"] != "") {
                    echo '<span id="metadata" data-filename="'.$metadata["filename"].'" data-extension=""></span>';
                } elseif ($metadata["extension"] != "") {
                    echo '<span id="metadata" data-filename="" data-extension="'.$metadata["extension"].'"></span>';
                }
            } else {
                echo "<script>$(document).ready(function(){ $('h1').text('invalid secret key'); });</script>";
            }
        } else {
            echo "<script>$(document).ready(function(){ $('h1').text('invalid secret key'); });</script>";
        }
        ?>
        <div id="bottom_link">
            <a href="/">upload new file</a>
        </div>
        <div id="bottom_link_open_tab">
            <a href="#" target="_blank">open in new tab</a>
        </div>
    </body>
</html>
<?php else: ?>
    <?php include("front_template_minified.html"); ?>
<?php endif; ?>
