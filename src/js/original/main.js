$(document).on('change', '#upfile', function() {
    $('input[type="submit"]').attr('disabled', false);
    var fileName = $(this).val();
    fileName = fileName.match(/[^\\/]*$/)[0];
    if (fileName == '') {
        fileName = 'choose a file (or drag and drop)';
        $('input[type="submit"]').hide();
        $('label.file-button').html('<span>choose a file</span><span>or drag and drop, or copy+v</span>');
    } else {
        $('input[type="submit"]').show();
        $('input[type="submit"]').val('upload file  →');
        $('#deletion_link').hide();
        $('#result').text('');
        $('input[type="submit"]').css('background', '');
        $('label.file-button').text(fileName);
    }
    if (getFileSize() > 5000000 && !(document.getElementById('encrypt_checkbox').checked)) {
        $('input[type="submit"]').val('max size 5 MB for unencrypted files');
        $('input[type="submit"]').attr('disabled', true);

    }
    if (getFileSize() > 30000000 && document.getElementById('encrypt_checkbox').checked) {
        $('input[type="submit"]').val('max size 30 MB for encrypted files');
        $('input[type="submit"]').attr('disabled', true);
    }
    if (!allowedFile()) {
        $('input[type="submit"]').val('jpg, jpeg, png and gif are only allowed for unencrypted uploads');
        $('input[type="submit"]').attr('disabled', true);
    }
});

function getFileSize() {
    var input = document.getElementById('upfile');
    var file = input.files[0];
    if (typeof(file) !== 'undefined') {
        return file.size;
    }
}

function getFileExtension() {
    var input = document.getElementById('upfile');
    var file = input.files[0];
    if (typeof(file) !== 'undefined') {
        return file.name.split('.').pop(); /* in the future, return mime instead of string manipulation */
    }
}

function getFilenameWithExtension() {
    var input = document.getElementById('upfile');
    var file = input.files[0];
    if (typeof(file) !== 'undefined') {
        return file.name;
    }
}

function allowedFile() {
    var input = document.getElementById('upfile');
    var file = input.files[0];
    if (typeof(file) !== 'undefined') {
        var type = file.type;
        if (document.getElementById('encrypt_checkbox').checked) {
            return true;
        } else if (['image/jpg', 'image/jpeg', 'image/png', 'image/gif'].includes(type)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function returnClipboardData(callback) {
   return callback(clipboard_data);
}

var base64_data;
function getBase64(data_in_clipboard, file, onLoadCallback) {
    if(data_in_clipboard) { /* already in base64 */
        returnClipboardData = onLoadCallback;
        returnClipboardData(clipboard_data);
    } else {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = onLoadCallback;
        reader.onerror = function(error) {
            alert('can not read the file');
        };
    }
}
    
$(document).on('submit', 'form', function(event) {
    event.preventDefault();
    var errors = false;
    if(document.getElementById('upfile').files.length == 0 || (getFileSize() > 5000000 && !(document.getElementById('encrypt_checkbox').checked)) || (getFileSize() > 30000000 && document.getElementById('encrypt_checkbox').checked) || !allowedFile()) {
        errors = true;
    }
    if(errors === true && clipboard_data === false) {
        alert('something went wrong');
    } else {
        if(document.getElementById('encrypt_checkbox').checked) {
            $('#encryption_notice').show();
            $('input[type="submit"]').val('encrypting... ');
            var data_from_clipboard = false;
            if(clipboard_data !== false) {
                data_from_clipboard = true;
            }
            getBase64(data_from_clipboard, document.getElementById('upfile').files[0], function(data) {
                if(typeof(data.target) == 'undefined') {
                    base64_data = data;
                } else {
                    base64_data = data.target.result;
                }
                if (key_length === 'custom') {
                    var encrypted = CryptoJS.AES.encrypt(base64_data, $('#user_password').val()).toString();
                } else {
                    var random_string = CryptoJS.lib.WordArray.random(key_length).toString();
                    var encrypted = CryptoJS.AES.encrypt(base64_data, random_string).toString();
                }
                if (deletion_time === 'custom') {
                    deletion_time = $('#user_deletion_time').val();
                }
                var metadata_extension = '';
                var metadata_filename = '';
                if($('#metadata').val() !== 'none') {
                    if($('#metadata').val() === 'extension') {
                        metadata_extension = getFileExtension();
                    } else if($('#metadata').val() === 'both') {
                        metadata_filename = getFilenameWithExtension();
                    }
                }
                if (typeof(random_string) === 'undefined' && key_length === 'custom' && $('#user_password').val() == '') {
                    alert('key can not be empty!');
                    $('#user_password').focus();
                    $('#encryption_notice').hide();
                    $('input[type="submit"]').val('upload file  →');
                } else {
                    $('#encryption_notice').hide();
                    $('input[type="submit"]').val('uploading file...');
                    $('.file-button, #upfile').attr('disabled', true);
                    $.ajax({
                        url: 'upload/file',
                        type: 'POST',
                        data: {
                            encrypting: 'true',
                            encrypted_base64: encrypted,
                            minutes: deletion_time,
                            metadata_ext: metadata_extension,
                            metadata_name: metadata_filename
                        },
                        xhr: function() {
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) {
                                myXhr.upload.addEventListener('progress', function(uploading) {
                                    if (uploading.lengthComputable) {
                                        $('#encryption_notice').hide();
                                        var loaded = uploading.loaded;
                                        var total = uploading.total;
                                        var percentage = loaded / total;
                                        percentage = Math.round(percentage * 1e2) / 1e2;
                                        var string_percentage = percentage * 100;
                                        string_percentage = Math.round(string_percentage * 1e2) / 1e2;
                                        string_percentage = string_percentage + '%'
                                        if (percentage < 1) {
                                            $('input[type="submit"]').val(string_percentage + ' uploaded');
                                            $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e ' + string_percentage + ', #d100ff3b ' + string_percentage + '');
                                        } else if (percentage === 1) {
                                            $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e 100%, #d100ff3b 100%');
                                            $('input[type="submit"]').val('waiting URL from the server...');
                                        }
                                    }
                                }, false);
                            }
                            return myXhr;
                        },
                        success: function(result) {
                            result = JSON.parse(result);
                            $('#encryption_notice').hide();
                            if (result.message === 'success') {
                                $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e 100%, #d100ff3b 100%');
                                $('input[type="submit"]').val('encrypted file upload successful');
                                $('input[type="submit"]').hide();
                                $('#deletion_link').show();
                                if (key_length === 'custom') {
                                    $('#result').html('<a  target="_blank" href="https://upload.vaa.red/' + result.fileId + '#' + $('#user_password').val() + '">https://upload.vaa.red/' + result.fileId + '#' + $('#user_password').val() + '</a>');
                                } else {
                                    $('#result').html('<a  target="_blank" href="https://upload.vaa.red/' + result.fileId + '#' + random_string + '">https://upload.vaa.red/' + result.fileId + '#' + random_string + '</a>');
                                }
                                $('#deletion_link').html('deletion link: <a href="https://upload.vaa.red/delete/' + result.deletionId + '">https://upload.vaa.red/delete/' + result.deletionId + '</a>');
                            } else {
                                $('input[type="submit"]').val(result.message);
                            }
                            $('.file-button, #upfile').attr('disabled', false);
                            $('label.file-button').html('<span>choose a file</span><span>or drag and drop, or copy+v</span>');
                        },
                        error: function() {
                            $('input[type="submit"]').val('something went wrong');
                            $('.file-button, #upfile').attr('disabled', false);
                            $('label.file-button').html('<span>choose a file</span><span>or drag and drop, or copy+v</span>');
                        }
                    });
                    clipboard_data = false;
                }
            });
        } else {
            $('input[type="submit"]').val('uploading file...');
            var form_data = new FormData($('form')[0]);
            $('.file-button, #upfile').attr('disabled', true);
            $.ajax({
                url: 'upload/file',
                type: 'POST',
                data: form_data,
                processData: false,
                cache: false,
                contentType: false,
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', function(uploading) {
                            if (uploading.lengthComputable) {
                                var loaded = uploading.loaded;
                                var total = uploading.total;
                                var percentage = loaded / total;
                                percentage = Math.round(percentage * 1e2) / 1e2;
                                var string_percentage = percentage * 100;
                                string_percentage = Math.round(string_percentage * 1e2) / 1e2;
                                string_percentage = string_percentage + '%'
                                if (percentage < 1) {
                                    $('input[type="submit"]').val(string_percentage + ' uploaded');
                                    $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e ' + string_percentage + ', #d100ff3b ' + string_percentage + '');
                                } else if (percentage == 1) {
                                    $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e 100%, #d100ff3b 100%');
                                    $('input[type="submit"]').val('waiting URL from the server...');
                                }
                            }
                        }, false);
                    }
                    return myXhr;
                },
                success: function(result) {
                    result = JSON.parse(result);
                    if (result.message === 'success') {
                        $('input[type="submit"]').css('background', 'linear-gradient(90deg, #d100ff9e 100%, #d100ff3b 100%');
                        $('input[type="submit"]').val('file upload successful');
                        $('input[type="submit"]').hide();
                        $('#result').html('<a target="_blank" href="' + result.url + '">' + result.url + '</a>');
                        $('#deletion_link').show();
                    } else {
                        $('input[type="submit"]').val(result.message);
                    }
                    $('#deletion_link').html('deletion link: <a href="https://upload.vaa.red/delete/' + result.deletionId + '">https://upload.vaa.red/delete/' + result.deletionId + '</a>');
                    $('.file-button, #upfile').attr('disabled', false);
                    $('label.file-button').html('<span>choose a file</span><span>or drag and drop, or copy+v</span>');
                },
                error: function() {
                    $('input[type="submit"]').val('something went wrong');
                    $('.file-button, #upfile').attr('disabled', false);
                    $('label.file-button').html('<span>choose a file</span><span>or drag and drop, or copy+v</span>');
                }
            });
            clipboard_data = false;
        }
    }
});

$(document).ready(function() {
    var fileInput = document.querySelector('#upfile');
    $.event.props.push('dataTransfer');
    $('.file-button').on({
        dragover: function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).addClass('highlight');
            $(this).css('border-style', 'dashed');
            return false;
        },
        dragleave: function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).removeClass('highlight');
            $(this).css('border-style', 'solid');
            return false;
        },
        drop: function(e) {
            e.stopPropagation();
            e.preventDefault();
            $(this).css('border-style', 'solid');
            var file = e.dataTransfer.files[0];
            fileInput.files = e.dataTransfer.files;
            var fileReader = new FileReader();

            var this_obj = $(this);

            fileReader.onload = (function(file) {
                return function(event) {
                    var filename = file.name;
                    $('input[type="submit"]').show();
                    $('input[type="submit"]').val('upload file  →');
                    $('#deletion_link').hide();
                    $('#result').text('');
                    $('input[type="submit"]').css('background', '');
                    $('label.file-button').text(filename);
                };
            })(file);
            fileReader.readAsDataURL(file);
            return false;
        }
    });

    $('#advanced span.advanced_label').on('click', function() {
        $('#advanced #settings').toggle();
        if ($('.advanced_label').text() === '[show advanced options]') {
            $('.advanced_label').text('[hide advanced options]');
        } else {
            $('.advanced_label').text('[show advanced options]');
        }
    });

});

var key_length = 16;
$(document).on('change', '#length', function() {
    key_length = $(this).val();
    if (key_length === 'custom') {
        $('#user_password').show();
        $('#user_password').focus();
    } else {
        $('#user_password').hide();
    }
});

var deletion_time = 525948;
$(document).on('change', '#deletion', function() {
    deletion_time = $(this).val();
    if (deletion_time === 'custom') {
        $('#user_deletion_time').show();
        $('#user_deletion_time').focus();
    } else {
        $('#user_deletion_time').hide();
    }
});

$(document).on('click', '#label_enc', function() {
    if (getFileSize() > 5000000 && !(document.getElementById('encrypt_checkbox').checked)) {
        $('input[type="submit"]').val('max size 5 MB for unencrypted files');
        $('input[type="submit"]').attr('disabled', true);
    } else if (getFileSize() < 5000000 && !(document.getElementById('encrypt_checkbox').checked)) {
        $('input[type="submit"]').attr('disabled', false);
        $('input[type="submit"]').val('upload file  →');
    }
    if (getFileSize() > 30000000 && document.getElementById('encrypt_checkbox').checked) {
        $('input[type="submit"]').val('max size 30 MB for encrypted files');
        $('input[type="submit"]').attr('disabled', true);
    } else if (getFileSize() < 30000000 && document.getElementById('encrypt_checkbox').checked) {
        $('input[type="submit"]').attr('disabled', false);
        $('input[type="submit"]').val('upload file  →');
    }
    if (!allowedFile()) {
        $('input[type="submit"]').val('jpg, jpeg, png and gif are only allowed for unencrypted uploads');
        $('input[type="submit"]').attr('disabled', true);
    } else if (allowedFile()) {
        $('input[type="submit"]').attr('disabled', false);
        $('input[type="submit"]').val('upload file  →');
    }
    if(document.getElementById('encrypt_checkbox').checked) {
        $('div#length_menu').show();
    } else {
        $('div#length_menu').hide();
    }
});

var clipboard_data = false;
$(document).on('paste', function (e) {
    e = e.originalEvent;
    for (var i = 0; i < e.clipboardData.items.length; i++) {
        clipboard_data = false;
        var kind = e.clipboardData.items[i].kind;
        var type = e.clipboardData.items[i].type;
        if (kind === "file" && (type === 'image/png' || type === 'image/jpeg' || type === 'image/pjpeg' || type === 'image/webp' || type === 'image/gif' || type === 'image/x-icon' || type === 'image/x-windows-bmp' || type === 'image/tiff' || type === 'image/x-tiff' || type === 'image/bmp')) {
            var imageFile = e.clipboardData.items[i].getAsFile();
            clipboard_data = e.clipboardData.items[i];
            var fileReader = new FileReader();
            fileReader.onload = function() {
                clipboard_data = fileReader.result;
                $('input[type="submit"]').attr('disabled', false);
                var fileName = 'image.png';
                $('input[type="submit"]').show();
                $('input[type="submit"]').val('upload file  →');
                $('#deletion_link').hide();
                $('#result').text('');
                $('input[type="submit"]').css('background', '');
                $('label.file-button').text(fileName);
            }
            fileReader.readAsDataURL(imageFile);
            break;
        }
    }
});
