# Usage
./upload.sh [File path] [true/false, to show deletion URL (default: false)] [integer, minutes after file will be deleted (min: 2, default: 0)] [true/false, to save file-extension (default: false)] [true/false, to save file extension and filename (default: false)]
For example: `upload.sh files/file.log false 60 true`
```
chmod +x upload.sh
```
```
./upload.sh image.png
```
You can make it to your $PATH, so you could just `upload image.png`, for example:
```
sudo mv upload.sh /usr/bin/upload
```
```
sudo chmod +x /usr/bin/upload
```
