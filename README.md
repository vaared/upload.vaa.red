# upload-vaa, client-side encrypted file host

 [https://upload.vaa.red](https://upload.vaa.red)
 
**upload-vaa** is a client-side encrypted file host. Upload any file you want.\* Image, audio and video files are automatically embedded. Try to upload an image for example!

Since the file is encrypted in the client-side (in user's browser), the contents of the file can't be viewed by the admins of the server. Not even filename or file type.

\* within legal limits of course

## Command-line API
There is a simple bash client available for uploading files: [api-client](api-client/).

## Self-hosting
If you want to self-host, here's a guide how to do it in linux-apache-mysql-php based system: [SELFHOST.md](SELFHOST.md).

## How it works
When user uploads a file, it will be encrypted in the user's browser using [CryptoJS](https://github.com/brix/crypto-js) library before it gets uploaded to the server. When user opens up an encrypted file, it will decrypted in the user's browser using the same CryptoJS library.

CryptoJS defaults to 256 bit key size for AES, PKCS#7 padding and CBC mode. AES has a 128 bit block size which is also the IV size. CryptoJS uses the non-standardized OpenSSL KDF for key derivation (EvpKDF) with MD5 as the hashing algorithm and 1 iteration. Since the MD5 is hard coded into the CryptoJS function used to derive the key from the passphrase, in this project [we are overwriting the default config](https://gitlab.com/vaared/upload.vaa.red/blob/master/src/js/original/crypto.js#L2139) and using SHA-256 instead. In newer versions of OpenSSL, SHA-256 is also used instead of MD5, so this ensures compatibility with the OpenSSL library.

## License
All the code is licensed under MIT.
